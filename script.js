let numFirst = parseInt(prompt("Введите первое число: "));
let numSecond = parseInt(prompt("Введите второе число: "));

while (isNaN(numFirst)) {
    alert("Первое число не является целым числом, введите целое число.")
    numFirst = parseInt(prompt("Введите первое число: "));
}

while (isNaN(numSecond)) {
    alert("Второе число не является целым числом, введите целое число.")
    numSecond = parseInt(prompt("Введите второе число: "));
}

let numbers = [numFirst, numSecond]
for (let i = 0; i < numbers.length - 1; i++) {
    for (let j = 0; j < numbers.length - 1 - i; j++) {
        if (numbers[j] > numbers[j + 1]) {
            let num = numbers[j];
            numbers[j] = numbers[j + 1];
            numbers[j + 1] = num;
        }
    }
}
console.log(numbers);

//2

let numThird = parseInt(prompt("Введите первое число: "));
let numFourth = parseInt(prompt("Введите второе число: "));

if (numThird % 2 === 0) {
    console.log(numThird + " - число является парным.");
} else {
    while (numThird % 2 !== 0) {
        alert("Первое число является не парным. Введите парное число.")
        numThird = parseInt(prompt("Введите первое число: "))
    }
    console.log(numThird + " - число является парным.");
}


if (numFourth % 2 === 0) {
    console.log(numFourth + " - число является парным.");
} else {
    while (numFourth % 2 !== 0) {
        alert("Второе число является не парным. Введите парное число.")
        numFourth = parseInt(prompt("Введите второе число: "))
    }
    console.log(numFourth + " - число является парным.");
}